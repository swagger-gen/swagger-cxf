# Swagger jaxrs-cxf generated server

```
java -jar swagger-codegen-cli-2.2.2.jar generate -i swagger.json -l jaxrs-cxf
```

A generált kód a jaxrs-resteasy-eap-ra hasonlít

Generálás utáni teendők:
* test mappa törlése, mert a tesztek nem futnak le hiányzó függőség miatt!!! 

Előnyök:
* Kevés generált kód (api, model, impl)
* Megfelelő válaszokkal generálja le az apit

```
    @POST
    @Path("/login")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", tags = {"Login"})
    public LoginResponseDTO login(LoginDTO loginDTO); 
```

Hátrányok:
* Válaszban nem lehet HTTP kódot megadni, csak az adott
* Nem sikerült meghívni...
