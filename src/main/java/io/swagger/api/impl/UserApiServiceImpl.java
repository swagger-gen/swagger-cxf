package io.swagger.api.impl;

import io.swagger.api.UserApi;
import io.swagger.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class UserApiServiceImpl implements UserApi {
    public List<UserDTO> getUsers() {
        List<UserDTO> users = new ArrayList<UserDTO>();
        for (int i = 0; i < 10; i++) {
            UserDTO user = new UserDTO();
            user.setId(i);
            user.setName("Teszt Elek " + i);
            users.add(user);
        }
        return users;
    }

}

