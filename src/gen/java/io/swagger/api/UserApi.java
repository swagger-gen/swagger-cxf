package io.swagger.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.model.UserDTO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/")
@Api(value = "/", description = "")
public interface UserApi {

    @GET
    @Path("/users")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", tags = {"User"})
    public List<UserDTO> getUsers();
}

