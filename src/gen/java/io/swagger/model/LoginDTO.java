package io.swagger.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class LoginDTO {

    @ApiModelProperty(example = "null", required = true, value = "")
    private String username = null;
    @ApiModelProperty(example = "null", required = true, value = "")
    private String password = null;

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    /**
     * Get username
     *
     * @return username
     **/
    @NotNull
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LoginDTO username(String username) {
        this.username = username;
        return this;
    }

    /**
     * Get password
     *
     * @return password
     **/
    @NotNull
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginDTO password(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class LoginDTO {\n");

        sb.append("    username: ").append(toIndentedString(username)).append("\n");
        sb.append("    password: ").append(toIndentedString(password)).append("\n");
        sb.append("}");
        return sb.toString();
    }
}

