package io.swagger.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class LoginResponseDTO {

    @ApiModelProperty(example = "null", required = true, value = "")
    private String name = null;
    @ApiModelProperty(example = "null", required = true, value = "")
    private String token = null;

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    /**
     * Get name
     *
     * @return name
     **/
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LoginResponseDTO name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get token
     *
     * @return token
     **/
    @NotNull
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginResponseDTO token(String token) {
        this.token = token;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class LoginResponseDTO {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    token: ").append(toIndentedString(token)).append("\n");
        sb.append("}");
        return sb.toString();
    }
}

