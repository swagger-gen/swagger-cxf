package io.swagger.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class UserDTO {

    @ApiModelProperty(example = "null", required = true, value = "")
    private Integer id = null;
    @ApiModelProperty(example = "null", required = true, value = "")
    private String name = null;

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    /**
     * Get id
     *
     * @return id
     **/
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDTO id(Integer id) {
        this.id = id;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserDTO {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("}");
        return sb.toString();
    }
}

